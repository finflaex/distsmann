﻿using DbLibrary;

namespace WebApplication1.Models
{
    public class EmployeesIndexModel
    {
        public Employee[] Employees { get; set; }
    }
}