﻿using System.Collections.Generic;
using DbLibrary;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication1.Models
{
    public class EmployeeEditModel
    {
        public Employee Employee { get; set; }
        public SelectList Posts { get; set; }
    }
}