﻿using DbLibrary;

namespace WebApplication1.Models
{
    public class PostsIndexModel
    {
        public Post[] Posts { get; set; }
    }
}