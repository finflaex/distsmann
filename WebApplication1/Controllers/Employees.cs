﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DbLibrary;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class Employees : Controller
    {
        private readonly DataBaseContext _db;

        public Employees(DataBaseContext db)
        {
            _db = db;
        }

        public async Task<IActionResult> Index()
        {
            return View(new EmployeesIndexModel
            {
                Employees = await _db.Employees.Include(v => v.Post).ToArrayAsync()
            });
        }

        public async Task<IActionResult> Edit(Guid id)
        {
            var employee = await _db.Employees.Where(v => v.Id == id).Include(v => v.Post).FirstAsync();
            var posts = new SelectList(_db.Posts, nameof(Post.Id), nameof(Post.Caption), employee.Post.Id);
            return View(new EmployeeEditModel
            {
                Employee = employee,
                Posts = posts,
            });
        }

        [HttpPost]
        public async Task<IActionResult> SaveEdit(
            [FromForm] EmployeeEditModel model)
        {
            _db.Update(model.Employee);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> New()
        {
            var employee = new Employee
            {
                FirstName = "Новый",
                Dob = DateTime.Now.AddYears(-20),
            };
            var posts = new SelectList(_db.Posts, nameof(Post.Id), nameof(Post.Caption));
            return View(new EmployeeEditModel
            {
                Employee = employee,
                Posts = posts,
            });
        }

        [HttpPost]
        public async Task<IActionResult> SaveNew(
            [FromForm] EmployeeEditModel model)
        {
            await _db.AddAsync(model.Employee);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            var record = _db.Employees.Find(id);
            _db.Employees.Remove(record);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}