﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DbLibrary.Extensions;
using Microsoft.EntityFrameworkCore;

namespace DbLibrary
{
    public sealed class DataBaseContext : DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options)
            : base(options)
        {
            Database.EnsureCreated();
            if (!Posts.Any())
                Posts.AddRange(new Post
                {
                    Caption = "Директор",
                    Employees = new[]
                    {
                        new Employee
                        {
                            LastName = "Иванов",
                            FirstName = "Иван",
                            MiddleName = "Иванович",
                            Dob = new DateTime(1970, 1, 1),
                            Salary = 200000
                        }
                    }
                }, new Post
                {
                    Caption = "Ведущий инженер-программист",
                    Employees = new[]
                    {
                        new Employee
                        {
                            LastName = "Казанцева",
                            FirstName = "Екатерина",
                            Dob = new DateTime(1988, 12, 03),
                            Salary = 100000
                        }
                    }
                }, new Post
                {
                    Caption = "инженер-программист",
                    Employees = new[]
                    {
                        new Employee
                        {
                            LastName = "Перевалов",
                            FirstName = "Вадим",
                            MiddleName = "Анатольевич",
                            Dob = new DateTime(1986, 10, 31),
                            Salary = 80000
                        }
                    }
                });

            SaveChanges();
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            Post.Builder(builder);
            Employee.Builder(builder);
        }
    }

    public class Employee
    {
        public Employee()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        [MyDisplay("Имя")] public string FirstName { get; set; }

        [MyDisplay("Отчество")] public string MiddleName { get; set; }

        [MyDisplay("Фамилия")] public string LastName { get; set; }

        [MyDisplay("Дата рождения")] 
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime Dob { get; set; }

        [MyDisplay("Ставка")] public decimal Salary { get; set; }

        [MyDisplay("Должность")] public Post Post { get; set; }

        public Guid PostId { get; set; }

        public static void Builder(ModelBuilder builder)
        {
            builder.Entity<Employee>().HasKey(v => v.Id);
            builder.Entity<Employee>().HasOne(v => v.Post)
                .WithMany(v => v.Employees)
                .HasPrincipalKey(v => v.Id)
                .HasForeignKey(v => v.PostId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }

    public class Post
    {
        public Post()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public string Caption { get; set; }
        public IEnumerable<Employee> Employees { get; set; }

        public static void Builder(ModelBuilder builder)
        {
            builder.Entity<Post>().HasKey(v => v.Id);
        }
    }
}