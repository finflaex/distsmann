﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace DbLibrary.Extensions
{
    public static class _
    {
        public static IEnumerable<MyDisplay> GetDisplays(
            this Type @this)
        {
            return @this
                .GetProperties()
                .Select(v => v.GetCustomAttributes().OfType<MyDisplay>().FirstOrDefault())
                .Where(v => v != null);
        }

        public static MyDisplay GetDisplay<T, TP>(
            this T @this,
            Expression<Func<T, TP>> prop)
        {
            var expression = (MemberExpression) prop.Body;
            return expression.Member.GetCustomAttributes<MyDisplay>().FirstOrDefault();
        }
    }
}