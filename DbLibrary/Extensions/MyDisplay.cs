﻿using System;

namespace DbLibrary.Extensions
{
    public class MyDisplay : Attribute
    {
        public MyDisplay(string caption)
        {
            Caption = caption;
        }

        public string Caption { get; set; }
    }
}